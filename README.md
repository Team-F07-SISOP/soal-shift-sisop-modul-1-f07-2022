## Anggota Kelompok

Mohammad Fadhil Rasyidin Parinduri // 5025201131

Farrel Emerson // 5025201082

Samuel // 5025201187

### Soal Shift
- [link](https://docs.google.com/document/d/13lHX4hO09jf07y_JFv0BpwunFL-om9eiNFDlIooSW3o/edit?usp=sharing)


Kendala:
1. Harus menyesuaikan lagi karena di linux, true = 0 dan false = 1
2. Sulit untuk testing code karna harus mempercepat / memperlambat waktu secara manual (Nomor 3)
3. Tidak mengetahui bagaimana cara menambah cronjob di script minute_log/aggregation, sehingga masih harus menambahkan cronjob secara manual dengan *crontab -e* (Nomor 3)

# Laporan Resmi
### Soal Nomor 1
- [main.sh](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-1-f07-2022/-/blob/main/soal1/main.sh)
- [register.sh](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-1-f07-2022/-/blob/main/soal1/register.sh)
#### Soal 1a
##### Source Code 1a
```shell
#readonly variables
readonly TRUE=0 
readonly FALSE=1
readonly KEY=sisopmodul1

#main code
verified=$FALSE

echo "-REGISTER-"

while :
do
  printf "Username: "
  read user
  printf "Password: "
  read -s passwd
  printf "\n"

#di sini ada conditional code (tapi untuk nanti)

case $verified in
    $TRUE) 
      encrypt=$(echo $passwd | openssl enc -e -des3 -base64 -pass pass:$KEY -pbkdf2)
      mkdir -p ./users/ && echo $user $encrypt  >> ./users/user.txt || echo $user $encrypt > ./users/user.txt;
      writeLog "INFO" $user 
      break ;; 
    $FALSE) printf "\nRegistration Failed. Try Again.\n\n" ;;
  esac

done

echo "Registration Success!!"
```
##### Cara Pengerjaan
Tujuan: Membuat script `register.sh` yang berguna untuk register user dan disimpan ke dalam file `./users/user.txt` dan sistem login dalam script `main.sh`
1. initialize `verified=$FALSE` agar memberitahu program bahwa belum terverifikasi
2. `read user` dan `read -s passwd` untuk meminta user input untuk user dan passwd.
3. kemudian dengan bash command `case` untuk `$verified` tentukan conditionals untuk kedua case, true dan false. kasus true akan melakukan password encryption (1b) dan di-append ke dalam `./users/user.txt` dan akan menjalankan fungsi `writeLog` dan menuliskan user info ke dalam `user.txt` (1c)

```shell
#mencari tahu user di user.txt ada di baris ke berapa
#return 0 jika tidak ditemukan
getUserRow(){
    if [ -f $FILE ]; then
        local val=$(awk -v str=$1 '
            BEGIN{
                i=0
            }

            {
                if(str == $1) {
                    i=NR
                }
            }

            END{print i}
        ' $FILE )

            return $val
    fi

    return 0
}

#decrypt password dan mengcompare password input dengan yang ada di user.txt
isMatchPasswd(){
    index=$1
    passwd=$2

    local hashPwd=$(awk -v i=$index 'FNR == i {print $NF}' $FILE)
    local realPwd=$(echo "$hashPwd" | openssl enc -d -des3 -base64 -pass pass:$KEY -pbkdf2)
    
    [[ $realPwd == $passwd ]] && return $TRUE || return $FALSE
}

echo "-LOGIN-"
userNow=""
passwdNow=""

while :
do
    printf "Username: "
    read user
    printf "Password: "
    read -s passwd
    printf "\n"

    getUserRow $user
    indexUser=$?

    if [[ $indexUser -eq 0 ]]; then
        printf "\nLogin Failed. Try Again.\n\n"
        continue
    fi


    isMatchPasswd $indexUser $passwd
    matchPasswd=$?

    case $matchPasswd in
    $TRUE) 
        writeLog "INFO" $user;
        printf "Login Success.\n\n"
        #userNow dipakai untuk command att/dl 
        userNow=$user
        passwdNow=$passwd
        break ;; 
    $FALSE)
        writeLog "ERROR" $user;
        printf "\nLogin Failed. Try Again.\n\n" ;;
    esac
done
```
##### Cara Pengerjaan
Tujuan: sistem login dalam `main.sh`
1. mencari tahu user di user.txt ada di baris ke berapa dengan `getUserRow` menggunakan awk dan akan return 0 apabila tidak ditemukan
2. karena script password yang dibuat mendekripsi password, maka password terutama akan di-decrypt kemudian dibandingkan dengan yang ada dalam `user.txt` dengan `isMatchPasswd` apabila semua conditionals masuk, maka akan return `$TRUE` apabila maka return `$FALSE`
3. sistem login pada `main.sh` akan read input username dan password dari user kemudian akan memanggil `getUserRow` dan membuat conditional apabila `$indexUser -eq 0` menandakan bahwa "Login Failed. Try Again"
4. case `$matchPasswd` dibuat untuk conditionals login user, apabila berhasil, maka akan `writeLog "INFO" $user` namun apabila gagal, akan `writeLog "ERORR" $user`

### Soal 1b
#### Source Code 1b
```shell
#function untuk mengecek panjang password
isMoreThan8(){
  local passwd=${#1}

  [[ $passwd -ge 8 ]] && return $TRUE || return $FALSE
}

#function untuk mengecek apakah ada minimal 1 uppercase & 1 lowercase
isContainsUpperAndLower(){
  local passwd=$1

  [[ "$passwd" =~ [[:upper:]] ]] && [[ "$passwd" =~ [[:lower:]] ]] && return $TRUE || return $FALSE
}


#function untuk mengecek tidak ada special character
isAlphaNumeric(){
  local passwd=$1

  [[ "$passwd" =~ ^[[:alnum:]]+$ ]] && [[ ! "$passwd" =~ ^[[:digit:]]+$ ]] && [[ "$passwd" =~ [0-9] ]] && return $TRUE || return $FALSE
}


#functiom untuk mengecek apakah username != password
isDifferentWithUsername(){
  local username=$1
  local passwd=$2

  [[ "$username" != "$passwd" ]] && return $TRUE || return $FALSE
}

isUserExists(){
  grep -q -w -s $1 ./users/user.txt && writeLog "ERROR" $1 && return $TRUE || return $FALSE
}

#gabungan 4 fungsi diatas ke dalam fungsi ini
verifyPassword(){
  local moreThan8=$1
  local upperAndLower=$2
  local alnum=$3
  local diff=$4 
  local user=$5

  if [ $moreThan8 == $TRUE -a $upperAndLower == $TRUE -a $alnum == $TRUE -a $diff == $TRUE -a $user == $FALSE ]; then 
    return $TRUE
  else 
    return $FALSE
  fi

  # di sini ada kode

  case $verified in
    $TRUE) 
      encrypt=$(echo $passwd | openssl enc -e -des3 -base64 -pass pass:$KEY -pbkdf2)
      mkdir -p ./users/ && echo $user $encrypt  >> ./users/user.txt || echo $user $encrypt > ./users/user.txt;
      writeLog "INFO" $user 
      break ;; 
    $FALSE) printf "\nRegistration Failed. Try Again.\n\n" ;;
  esac
}
```
#### Cara Pengerjaan
Tujuan: membuat password tertutup/hidden dan password memiliki kriteria sebagai berikut.
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username
1. membuat fungsi conditionals untuk tiap-tiap kriteria {isMoreThan8 : untuk mengecek apakah lebih/kurang dari 8 karakter; isContainsUpperAndLower : untuk mengecek apakah memiliki minimal 1 huruf kapital dan kecil; isAlphaNumeric : untuk mengecek apakah  alphanumeric dan tidak memiliki special characters; isDifferentWithUsername : untuk mengecek apakah berbeda dengan username}
2. membuat fungsi `isUserExist` dengan `grep` untuk mengecek apakah user dengan username yang diinput user sudah ada di dalam `user.txt`
3. fungsi `verifyPassword` yang mengandung semua conditional functions untuk mengecek password yang diinput oleh user, apabila password melewati semua conditionals return `$TRUE` apabila tidak return `$FALSE`
4. case verified untuk enkripsi apabila `$TRUE` password akan terenkripsi dan kalau `$FALSE` akan echo "Registration Failed. Try Again"

### Soal 1c
#### Source Code 1c
```shell
checkAttempt(){
    local u=$1

    local success=$(grep -c "LOGIN:INFO User $u logged in" $LOG)
    local failed=$(grep -c "LOGIN:ERROR Failed login attempt on user $u" $LOG)

    # printf "user:$user\nsuccess: $success\nfailed: $failed\n\n"
    printf "user saat ini:$user\n"
    printf "total attempt: %d\n\n" $(($success + $failed))

    # let totalAttempt=$success+$failed
    
    # return totalAttempt
}
```
#### Cara Pengerjaan
Tujuan: `Register.sh` membuat log User already exists dan USERNAME registered successfully
1. fungsi `checkAttempt` memiliki dua case apabila `success` dan `failed` keduanya menggunakan fungsi `grep -c` kepada `$u` dan menuliskannya ke dalam `$LOG`
2. printf LOG

```shell
writeLog(){
  message=$1
  user=$2

  # MM/DD/YY hh:mm:ss
  now=`date +%m/%d/%y\ %H:%M:%S`
  
  case "$message" in
    "ERROR") echo "$now REGISTER:$message User already exists" >> ./log.txt || echo $user > ./log.txt ;;
    "INFO") echo "$now REGISTER:$message User $user registered successfully" >> ./log.txt || echo $user > ./log.txt ;;
  esac
}
```
#### Cara Pengerjaan
Tujuan: `main.sh` membuat log Failed login attempt on user dan User USERNAME logged in
1. `writeLog` akan memiliki tiga variabel yaitu `message`, `now`, dan `user`.
2. `now` merupakan variabel yang menyimpan data tanggal yang akan digunakan untuk log
3. membuat case apabila dipanggil "ERROR" dan "INFO" dalam `$message` yang kemudian akan di-append ke dalam `./log.txt`

### Soal 1d
#### Source Code 1d
```shell
downloadImg(){
    local sum=$2
    local filename=$(printf "%s_%s" `date +%Y-%m-%d` $3)
    local passwd=$4

    if ! [[ "$sum" =~ ^[0-9]+$ ]]; then 
        echo "Not a number"
        return
    fi

    sudo apt-get wget

    # jika ternyata file nya sudah ada, unzip, tambahkan pic, zip lagi
    if [ -e "$filename" ]; then
        lastFileOrder=$(ls ./$filename | tail -1 | awk -F'_' '{print $2}' || echo 0)
        unzip -o $filename.zip -P $passwd
        
        for(( i=1; i<=$sum; i++ ))
            do
                order=$(printf "%02d" $(($lastFileOrder+$i)))
                wget -O ./$filename/PIC_$order https://loremflickr.com/320/240
            done
        
        zip $filename.zip -r $filename -P $passwd
        printf "\nTasks Done Successfully.\n\n"
    
    # jika file nya belum ada, create dir, tambahkan pic, zip
    else
        mkdir -p ./$filename

        for(( i=1; i <= $sum; i++ ))
            do
                order=$(printf "%02d" $i)
                wget -O ./$filename/PIC_$order https://loremflickr.com/320/240
            done
        
        zip $filename.zip -r $filename -P $passwd
        printf "\nTasks Done Successfully.\n\n"
    fi

}
```
Tujuan: mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user, kemudian dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan.  folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut.
1. dalam fungsi `downloadImg` terdapat variabel `sum` untuk menyimpan angka `n pada dl n`
2. filename akan ditulis sesuai format
3. `$passwd` akan digunakan untuk memberikan passwd .zip file
4. utility `wget` akan menggampangkan task yang diperlukan
5. conditionals bekerja dengan cara pengecekan file dari order terakhir apakah ada atau tidak, apabila tidak ada akan membuat directory, tambahkan pict, kemudian zip.

```shell
checkAttempt(){
    local u=$1

    local success=$(grep -c "LOGIN:INFO User $u logged in" $LOG)
    local failed=$(grep -c "LOGIN:ERROR Failed login attempt on user $u" $LOG)

    # printf "user:$user\nsuccess: $success\nfailed: $failed\n\n"
    printf "user saat ini:$user\n"
    printf "total attempt: %d\n\n" $(($success + $failed))

    # let totalAttempt=$success+$failed
    
    # return totalAttempt
}
```
Tujuan: command `att` yang menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini
1. fungsi `checkAttempt` memiliki dua variabel yaitu `success` dan `failed` di mana kedua fungsi tersebut akan `grep -c` dan mengakses info yang ada di dalam `$LOG`
2. terpanggilnya salah satu dari dua case tersebut setelah user input `att` akan menunjukkan string yang sesuai.
```shell
#command = att / dl N, N = number
while :
do
    read cmd cmd2

    if [ $cmd == "att" ]; then
        checkAttempt $userNow
    elif [ $cmd == "dl" ]; then
        downloadImg $cmd $cmd2 $userNow $passwdNow
    else
        echo "command not found"
    fi
done
```
Tujuan: menginisiasi fungsi terhadap script
1. main function untuk memanggil `att` dan `dl` akan terutama menerima dua input dari user
2. conditionals untuk memanggil salah satu dari `att` dan `dl` yang kemudian menjalankan fungsi yang dibuat untuk kedua kondisi tersebut
3. apabila user meng-input tidak dari keduanya maka script akan menjalankan `echo "command not found"`

### Soal Nomor 2
- [soal2_forensic_dapos.sh](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-1-f07-2022/-/blob/main/soal2/soal2_forensic_dapos.sh)

#### Soal 2a
##### Source Code 2a
```shell
mkdir -p forensic_log_website_daffainfo_log
```
Tujuan: Membuat directory
##### Cara Pengerjaan:
1. `makedir -p` Jika belum ada, membuat directory `forensic_log_website_daffainfo_log`, jika sudah ada, dibiarkan

#### Soal 2b
##### Source Code 2b
```shell
average=$(awk -F ":" '
BEGIN{
    total=0; divider=0
} 
FNR == 1 {next} 
{ count[$3]++ }; 
{ hour[$3]++ } 
END { for (i in hour) total+=hour[i] } 
END{ for (i in hour) divider++ } 
END{ print "Rata-rata serangan adalah sebanyak", total/--divider, "requests per jam" }' log_website_daffainfo.log )

echo $average > ./forensic_log_website_daffainfo_log/ratarata.txt

```
Tujuan: Mencari rata rata serangan per jam
##### Cara Pengerjaan
1. Menggunakan awk untuk mengakses data pada `log_website_daffainfo.log`
2. Lalu membagi kolom menggunakan tanda `:` 
3. Karena kita tidak menggunakan line pertama pada log file, maka kita mengabaikan baris  pertama menggunakan `FNR == 1 {next}`
3.  lalu total serangan dibagi 12 jam dan dimasukan pada file `ratarata.txt`

#### Soal 2c
##### Source Code 2c
```shell
result=$(awk -F ':' '
BEGIN{
    count=0
    attackerIP=""
}
{ data[$1]++ } 
END{ 
    for (ip in data) 
        if(data[ip] > count) {
            count=data[ip];
            attackerIP=ip;
        } 
} 
END {
    print "IP yang paling banyak mengakses server adalah:", attackerIP, "sebanyak", count, "requests"
}' log_website_daffainfo.log )

echo $result > ./forensic_log_website_daffainfo_log/result.txt

```
Tujuan: Mencari ip yang melakukan serangan terbanyak
##### Cara Pengerjaan

1. Menggunakan awk untuk mengakses data pada `log_website_daffainfo.log`, lalu membagi kolom menggunakan tanda `:` 
2. Memasukan data ip yang berada pada kolom pertama awk ke dalam array menggunakan `$1`
3. Menghitung berapa kali setiap ip muncul
4. Menggunakan for-loop untuk mencari ip dengan count terbanyak
5. output dimasukan pada file result.txt

### Soal 2d
#### Source Code 2d
```shell
curl_req=$(awk -F ':' '
BEGIN{
    total=0
}
{ 
    if(index($NF, "curl") != 0){
        total++;
    }
}
END{ print "Ada", total, "requests yang menggunakan curl sebagai user-agent" }' log_website_daffainfo.log)

echo $curl_req >> ./forensic_log_website_daffainfo_log/result.txt
```
Tujuan: Mencari request yang menggunakan user-agent curl
##### Cara Pengerjaan

1. Menggunakan awk untuk mengakses data pada `log_website_daffainfo.log`, lalu membagi kolom menggunakan tanda `:` 
2. Kita mengecek apabila di kolom terakhir terdapat kata `curl` menggunakan `if(index($NF, "curl") != 0)`
3. Apa bila terdapat `curl` pada akhir line, maka kita hitung dan masukan ke dalam `result.txt`

### Soal 2e
#### Source Code 2e
```shell
ip_addr=$(awk -F [:/] '
{ 
    if($2 == "\"22" && $5 == 02)
        all_ip[$1]=$1
}
END{ 
    for(i in all_ip) printf "%s ", all_ip[i]
}
' log_website_daffainfo.log)

# IFS=split string ' '
IFS=' '

# all_ip akan membaca $ip_addr sebagai array
# dan setiap indeks ditentukan dari separator IFS
read -ra all_ip <<< "$ip_addr"

for i in "${all_ip[@]}"; do
    echo "$i Jam 2 pagi" >> ./forensic_log_website_daffainfo_log/result.txt
done

```
Tujuan: Mencari ip yang mengirimkan request pada jam 2 dan tanggal 22
##### Cara Pengerjaan

1. Menggunakan awk untuk mengakses data pada `log_website_daffainfo.log`, lalu membagi kolom menggunakan tanda `:` dan `/`
2. Mengecek setiap line dengan `$2` untuk mengecek `"22` dan `$5` untuk mengecek `02` pada kolom ke 2 dan 5 pada awk
3. Setelah awk selesai, hasil awk dimasukan dalam bentuk string, string tersebut akan dimasukan ke array menggungakan `IFS=` ``
4. String tersebut dipisah berdasarkan space dan dimasukan ke `$ip_addr[]` menggunakan `read -ra all_ip <<< "$ip_addr"`
5. Setelah itu setiap ip dimasukan ke `result.txt`


### Soal Nomor 3
- [minute_log.sh](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-1-f07-2022/-/blob/main/soal3/minute_log.sh)
- [aggregate_minutes_to_hourly_log.sh](https://gitlab.com/Team-F07-SISOP/soal-shift-sisop-modul-1-f07-2022/-/blob/main/soal3/aggregate_minutes_to_hourly_log.sh)

#### Soal 3a
Tujuan : Memasukkan semua hasil dari `du -sh <target-path>` dan `free -m` ke dalam 1 file dengan format nama tertentu

##### Source Code 3a
```shell
readonly DATE_NOW=$(date "+%F%T" | tr -d "-" | tr -d ":")
readonly LOG_DIR=$HOME/log
readonly LOG_STARTER_STR="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

mkdir -p $LOG_DIR

datenow=$(date "+%F%T" | tr -d "-" | tr -d ":")
filename="metrics_"$datenow".log"

touch $LOG_DIR/$filename

ram=$(
    free -m | 
    awk 'NR!=1 {str=$2; for(i=3;i<=NF;i++){str=str" "$i}; print str}' |
    tr " " , | tr "\n" ,
)

path_size=$(du -sh $HOME | awk '{print $2","$1}')

echo $LOG_STARTER_STR >> $LOG_DIR/$filename 
echo "$ram$path_size" >> $LOG_DIR/$filename
```
#### Cara Pengerjaan
1. Membuat directory dengan nama log di $HOME/log dengan mkdir -p. $HOME sendiri berarti /home/{user} dari tiap user.
2. Membuat file dengan touch, nama file nya sendiri adalah metrics_< tanggal tanpa tanda : ataupun - >.log
3. Memanggil `free -m` dan memanipulasi output nya dengan AWK.

![free -m](https://user-images.githubusercontent.com/89601859/156915349-3119181b-4015-48fd-be50-e42dce2c5ff7.png)

Dari gambar diatas, dapat dilihat jika kita hanya membutuhkan data angka nya saja. Berarti dalam AWK, kita tidak butuh row ke 1, dan mulai loop dari 3 sampai ke akhir(NF). Lalu print dengan spasi sebagai separator nya. Lalu remove spasi dan newline dan replace dengan tanda koma (,)

4. Memanggil `du -sh $HOME` dan print secara terbalik dengan AWK (sesuai dengan output yang diminta di soal)
5. Masukkan semua ke dalam file dengan `mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size` dimasukkan terlebih dahulu, baru data - data nya.

### Soal 3b
Tujuan : Melakukan cronjob tiap menit dalam crontab

#### Source Code 3b
```shell
is_cron_created=$(crontab -l | grep minute_log.sh)

# create crontab 
if [ -z "$is_cron_created" ]; then
    crontab -l > crontemp
    echo "* * * * * /bin/bash $PWD/minute_log.sh >/dev/null 2>&1" >> crontemp
    crontab crontemp
    rm crontemp

    echo "cron created"
fi
```
#### Cara Pengerjaan
1. Mengecek apakah script minute_log.sh sudah ada dalam crontab dengan grep.
2. Jika belum ada, tulis semua cronjob yang ada di crontab ke sebuah file dengan nama crontemp.
3. Tuliskan script untuk melakukan cronjob tiap menit untuk file minute_log.sh ke crontemp
4. Masukkan crontemp ke dalam crontab dan remove crontemp


### Soal 3c
Tujuan : Membuat agregasi file log ke satuan jam dengan format nama tertentu, dengan isi file nilai rata-rata, maximum, dan minimum

#### Source Code 3c
```shell
readonly DATE_NOW=$(date --date="1 hour ago" "+%F%H" | tr -d "-" | tr -d ":")
readonly LOG_DIR=$HOME/log
readonly LOG_STARTER="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

filename="metrics_agg_$DATE_NOW.log"

pattern="metrics_$DATE_NOW"

all_file=$(ls $LOG_DIR | grep $pattern | tr "\n" " ")

if [ -z "$all_file" ]; then
    echo "No log file 1 hour ago"
    exit 1
fi

IFS=' '
read -ra logfile <<< "$all_file"
```
Penjelasan :
1. Membuat nama file dengan `DATE_NOW` di set 1 hour ago
2. Mengambil file dengan nama tanggal yang sama dengan `DATE_NOW`, lalu mengganti output newline dengan spasi
3. Jika tidak ada file dengan nama pattern yang sama, exit program
4. Masukkan hasil nya ke dalam array logfile, dengan `IFS=' '`. Artinya data yang masuk ke array per index dibedakan per spasi yang muncul 


```shell
maximum=(0 0 0 0 0 0 0 0 0)
minimum=(100000 100000 100000 100000 100000 100000 100000 100000 100000)
average=(0 0 0 0 0 0 0 0 0)
awk_path=""
awk_path_size=""

first_iteration=1
count=0

for i in "${logfile[@]}"; do
    ((count++));
    awk_path=$(awk -F "," 'FNR==2 {print $10}' $LOG_DIR/$i);
    awk_path_size=$(awk -F "," 'FNR==2 {print $11}' $LOG_DIR/$i);

    if [ $first_iteration == 1 ]; then
        maximum[0]=$(awk -F "," 'FNR==2 {print $1}' $LOG_DIR/$i)
        minimum[0]=${maximum[0]}
        average[0]=${maximum[0]}

        maximum[1]=$(awk -F "," 'FNR==2 {print $2}' $LOG_DIR/$i)
        minimum[1]=${maximum[1]}
        average[1]=${maximum[1]}

        maximum[2]=$(awk -F "," 'FNR==2 {print $3}' $LOG_DIR/$i)
        minimum[2]=${maximum[2]}
        average[2]=${maximum[2]}

        ...

        maximum[8]=$(awk -F "," 'FNR==2 {print $9}' $LOG_DIR/$i)
        minimum[8]=${maximum[8]}
        average[8]=${maximum[8]}

        first_iteration=0

    else 
        mem_total=$(awk -F "," 'FNR==2 {print $1}' $LOG_DIR/$i)
        average[0]=$((${average[0]} + $mem_total)) 

        mem_used=$(awk -F "," 'FNR==2 {print $2}' $LOG_DIR/$i)
        average[1]=$((${average[1]} + $mem_used)) 

        mem_free=$(awk -F "," 'FNR==2 {print $3}' $LOG_DIR/$i)
        average[2]=$((${average[2]} + $mem_free)) 

        ...

        swap_free=$(awk -F "," 'FNR==2 {print $9}' $LOG_DIR/$i)
        average[8]=$((${average[8]} + $swap_free)) 

        if [[ $mem_total -ge maximum[0] ]]; then
            maximum[0]=$mem_total
        elif [[ $mem_total -lt minimum[0] ]]; then
            minimum[0]=$mem_total
        fi

        if [[ $mem_used -ge maximum[1] ]]; then
            maximum[1]=$mem_used
        elif [[ $mem_used -lt minimum[1] ]]; then
            minimum[1]=$mem_used
        fi

        if [[ $mem_free -ge maximum[2] ]]; then
            maximum[2]=$mem_free
        elif [[ $mem_free -lt minimum[2] ]]; then
            minimum[2]=$mem_free
        fi

        ...

        if [[ $swap_free -ge maximum[8] ]]; then
            maximum[8]=$swap_free
        elif [[ $swap_free -lt minimum[8] ]]; then
            minimum[8]=$swap_free
        fi
    fi
done
```
Penjelasan :
1. Membuat array untuk maximum, minimum, dan rata - rata dengan panjang 9. Untuk maximum dan average diset 0, sementara minimum diset nilai besar
2. Untuk setiap file log yang sudah ada, lakukan AWK dan ambil nilai angka yang terletak di FNR == 2. Untuk urutan setiap index mewakili `"mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free"`.
3. Jika ini masuk iterasi pertama, tidak perlu membandingkan nilai untuk maksimum dan minimum, sementara untuk rata-rata masukkan saja nilainya ke dalam array
4. Jika bukan iterasi pertama, bandingkan nilai yang ada sekarang dengan nilai sebelumnya tiap array, sementara untuk rata-rata tetap tambahkan nilai setiap data yang masuk
5. Setiap iterasi yang ada, lakukan `count++` untuk mencari berapa kali perulangan dilakukan. Dan masukkan 2 data sisa (path dan path_size) ke variable awk_path dan awk_path_size.


```shell
mkdir -p $LOG_DIR && touch $LOG_DIR/$filename

echo $LOG_STARTER >> $LOG_DIR/$filename

minimum_output="minimum,"
maximum_output="maximum,"
average_output="average,"

for maxi in ${maximum[@]}; do
    maximum_output+="$maxi,"
done
maximum_output+="$awk_path,$awk_path_size"


for mini in ${minimum[@]}; do
    minimum_output+="$mini,"
done
minimum_output+="$awk_path,$awk_path_size"


for avg in ${average[@]}; do
    avg=$(echo $(($avg / $count))) 
    average_output+="$avg,"
done
average_output+="$awk_path,$awk_path_size"

echo $minimum_output >> $LOG_DIR/$filename
echo $maximum_output >> $LOG_DIR/$filename
echo $average_output >> $LOG_DIR/$filename
```
Penjelasan :
1. Masukkan tulisan `"type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"` ke file
2. Karena di contoh output yang diminta terdapat type (maximum,minimum,average), maka tambahkan type yang diminta terlebih dahulu di masing - masing variable minimum_output, maximum_output, dan average_output
3. Lakukan loop untuk array maximum dan minimum, dan masukkan nilainya ke dalam variable 
4. Untuk rata-rata, setiap nilainya bagi dahulu dengan count (yang sudah dihitung di perulangan sebelumnya) dan masukkan nilainya ke variable
5. `echo` masing - masing output ke file

### Soal 3d
Tujuan : Membuat semua file log hanya dapat dibaca oleh user pemilik file

#### Source Code 3d
```shell
chmod 400 $LOG_DIR/$filename
```
Penjelasan :
`chmod 400` sendiri berarti user/owner hanya dapat membaca file, tetapi tidak dapat write / execute file nya (Referensi : [chmod 400](https://chmodcommand.com/chmod-400/))
