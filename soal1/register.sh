#!/bin/bash

#readonly variables
readonly TRUE=0 
readonly FALSE=1
readonly KEY=sisopmodul1

#function untuk mengecek panjang password
isMoreThan8(){
  local passwd=${#1}

  [[ $passwd -ge 8 ]] && return $TRUE || return $FALSE
}

#function untuk mengecek apakah ada minimal 1 uppercase & 1 lowercase
isContainsUpperAndLower(){
  local passwd=$1

  [[ "$passwd" =~ [[:upper:]] ]] && [[ "$passwd" =~ [[:lower:]] ]] && return $TRUE || return $FALSE
}


#function untuk mengecek tidak ada special character
isAlphaNumeric(){
  local passwd=$1

  [[ "$passwd" =~ ^[[:alnum:]]+$ ]] && [[ ! "$passwd" =~ ^[[:digit:]]+$ ]] && [[ "$passwd" =~ [0-9] ]] && return $TRUE || return $FALSE
}


#functiom untuk mengecek apakah username != password
isDifferentWithUsername(){
  local username=$1
  local passwd=$2

  [[ "$username" != "$passwd" ]] && return $TRUE || return $FALSE
}

isUserExists(){
  grep -q -w -s $1 ./users/user.txt && writeLog "ERROR" $1 && return $TRUE || return $FALSE
}

#gabungan 4 fungsi diatas ke dalam fungsi ini
verifyPassword(){
  local moreThan8=$1
  local upperAndLower=$2
  local alnum=$3
  local diff=$4 
  local user=$5

  if [ $moreThan8 == $TRUE -a $upperAndLower == $TRUE -a $alnum == $TRUE -a $diff == $TRUE -a $user == $FALSE ]; then 
    return $TRUE
  else 
    return $FALSE
  fi
}

writeLog(){
  message=$1
  user=$2

  # MM/DD/YY hh:mm:ss
  now=`date +%m/%d/%y\ %H:%M:%S`
  
  case "$message" in
    "ERROR") echo "$now REGISTER:$message User already exists" >> ./log.txt || echo $user > ./log.txt ;;
    "INFO") echo "$now REGISTER:$message User $user registered successfully" >> ./log.txt || echo $user > ./log.txt ;;
  esac
}

#main code
verified=$FALSE

echo "-REGISTER-"

while :
do
  printf "Username: "
  read user
  printf "Password: "
  read -s passwd
  printf "\n"

  isMoreThan8 $passwd
  moreThan8=$?

  isContainsUpperAndLower $passwd
  upperAndLower=$?

  isAlphaNumeric $passwd
  alnum=$?

  isDifferentWithUsername $user $passwd
  diff=$?

  isUserExists $user
  userExists=$?

  verifyPassword $moreThan8 $upperAndLower $alnum $diff $userExists
  verified=$?

  case $verified in
    $TRUE) 
      encrypt=$(echo $passwd | openssl enc -e -des3 -base64 -pass pass:$KEY -pbkdf2)
      mkdir -p ./users/ && echo $user $encrypt  >> ./users/user.txt || echo $user $encrypt > ./users/user.txt;
      writeLog "INFO" $user 
      break ;; 
    $FALSE) printf "\nRegistration Failed. Try Again.\n\n" ;;
  esac

done

echo "Registration Success!!"