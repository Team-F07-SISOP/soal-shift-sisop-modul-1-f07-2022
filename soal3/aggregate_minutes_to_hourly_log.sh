#!/bin/bash

readonly DATE_NOW=$(date --date="1 hour ago" "+%F%H" | tr -d "-" | tr -d ":")
readonly LOG_DIR=$HOME/log
readonly LOG_STARTER="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

is_cron_created=$(crontab -l | grep aggregate_minutes_to_hourly_log.sh)

# create crontab 
if [ -z "$is_cron_created" ]; then
    crontab -l > crontemp
    echo "0 * * * * /bin/bash $PWD/aggregate_minutes_to_hourly_log.sh >/dev/null 2>&1" >> crontemp
    crontab crontemp
    rm crontemp

    echo "cron created"
fi


filename="metrics_agg_$DATE_NOW.log"

pattern="metrics_$DATE_NOW"

all_file=$(ls $LOG_DIR | grep $pattern | tr "\n" " ")

if [ -z "$all_file" ]; then
    echo "No log file 1 hour ago"
    exit 1
fi

IFS=' '
read -ra logfile <<< "$all_file"

maximum=(0 0 0 0 0 0 0 0 0)
minimum=(100000 100000 100000 100000 100000 100000 100000 100000 100000)
average=(0 0 0 0 0 0 0 0 0)
awk_path=""
awk_path_size=""

first_iteration=1
count=0

for i in "${logfile[@]}"; do
    ((count++));
    awk_path=$(awk -F "," 'FNR==2 {print $10}' $LOG_DIR/$i);
    awk_path_size=$(awk -F "," 'FNR==2 {print $11}' $LOG_DIR/$i);

    if [ $first_iteration == 1 ]; then
        maximum[0]=$(awk -F "," 'FNR==2 {print $1}' $LOG_DIR/$i)
        minimum[0]=${maximum[0]}
        average[0]=${maximum[0]}

        maximum[1]=$(awk -F "," 'FNR==2 {print $2}' $LOG_DIR/$i)
        minimum[1]=${maximum[1]}
        average[1]=${maximum[1]}

        maximum[2]=$(awk -F "," 'FNR==2 {print $3}' $LOG_DIR/$i)
        minimum[2]=${maximum[2]}
        average[2]=${maximum[2]}

        maximum[3]=$(awk -F "," 'FNR==2 {print $4}' $LOG_DIR/$i)
        minimum[3]=${maximum[3]}
        average[3]=${maximum[3]}

        maximum[4]=$(awk -F "," 'FNR==2 {print $5}' $LOG_DIR/$i)
        minimum[4]=${maximum[4]}
        average[4]=${maximum[4]}

        maximum[5]=$(awk -F "," 'FNR==2 {print $6}' $LOG_DIR/$i)
        minimum[5]=${maximum[5]}
        average[5]=${maximum[5]}

        maximum[6]=$(awk -F "," 'FNR==2 {print $7}' $LOG_DIR/$i)
        minimum[6]=${maximum[6]}
        average[6]=${maximum[6]}

        maximum[7]=$(awk -F "," 'FNR==2 {print $8}' $LOG_DIR/$i)
        minimum[7]=${maximum[7]}
        average[7]=${maximum[7]}

        maximum[8]=$(awk -F "," 'FNR==2 {print $9}' $LOG_DIR/$i)
        minimum[8]=${maximum[8]}
        average[8]=${maximum[8]}

        first_iteration=0

    else 
        mem_total=$(awk -F "," 'FNR==2 {print $1}' $LOG_DIR/$i)
        average[0]=$((${average[0]} + $mem_total)) 

        mem_used=$(awk -F "," 'FNR==2 {print $2}' $LOG_DIR/$i)
        average[1]=$((${average[1]} + $mem_used)) 

        mem_free=$(awk -F "," 'FNR==2 {print $3}' $LOG_DIR/$i)
        average[2]=$((${average[2]} + $mem_free)) 

        mem_shared=$(awk -F "," 'FNR==2 {print $4}' $LOG_DIR/$i)
        average[3]=$((${average[3]} + $mem_shared)) 

        mem_buff=$(awk -F "," 'FNR==2 {print $5}' $LOG_DIR/$i)
        average[4]=$((${average[4]} + $mem_buff)) 

        mem_available=$(awk -F "," 'FNR==2 {print $6}' $LOG_DIR/$i)
        average[5]=$((${average[5]} + $mem_available)) 

        swap_total=$(awk -F "," 'FNR==2 {print $7}' $LOG_DIR/$i)
        average[6]=$((${average[6]} + $swap_total)) 

        swap_used=$(awk -F "," 'FNR==2 {print $8}' $LOG_DIR/$i)
        average[7]=$((${average[7]} + $swap_used)) 

        swap_free=$(awk -F "," 'FNR==2 {print $9}' $LOG_DIR/$i)
        average[8]=$((${average[8]} + $swap_free)) 

        if [[ $mem_total -ge maximum[0] ]]; then
            maximum[0]=$mem_total
        elif [[ $mem_total -lt minimum[0] ]]; then
            minimum[0]=$mem_total
        fi

        if [[ $mem_used -ge maximum[1] ]]; then
            maximum[1]=$mem_used
        elif [[ $mem_used -lt minimum[1] ]]; then
            minimum[1]=$mem_used
        fi

        if [[ $mem_free -ge maximum[2] ]]; then
            maximum[2]=$mem_free
        elif [[ $mem_free -lt minimum[2] ]]; then
            minimum[2]=$mem_free
        fi

        if [[ $mem_shared -ge maximum[3] ]]; then
            maximum[3]=$mem_shared
        elif [[ $mem_shared -lt minimum[3] ]]; then
            minimum[3]=$mem_shared
        fi

        if [[ $mem_buff -ge maximum[4] ]]; then
            maximum[4]=$mem_buff
        elif [[ $mem_buff -lt minimum[4] ]]; then
            minimum[4]=$mem_buff
        fi

        if [[ $mem_available -ge maximum[5] ]]; then
            maximum[5]=$mem_available
        elif [[ $mem_available -lt minimum[5] ]]; then
            minimum[5]=$mem_available
        fi

        if [[ $swap_total -ge maximum[6] ]]; then
            maximum[6]=$swap_total
        elif [[ $swap_total -lt minimum[6] ]]; then
            minimum[6]=$swap_total
        fi

        if [[ $swap_used -ge maximum[7] ]]; then
            maximum[7]=$swap_used
        elif [[ $swap_used -lt minimum[7] ]]; then
            minimum[7]=$swap_used
        fi

        if [[ $swap_free -ge maximum[8] ]]; then
            maximum[8]=$swap_free
        elif [[ $swap_free -lt minimum[8] ]]; then
            minimum[8]=$swap_free
        fi


    fi
done

mkdir -p $LOG_DIR && touch $LOG_DIR/$filename

echo $LOG_STARTER >> $LOG_DIR/$filename

minimum_output="minimum,"
maximum_output="maximum,"
average_output="average,"

for maxi in ${maximum[@]}; do
    maximum_output+="$maxi,"
done
maximum_output+="$awk_path,$awk_path_size"


for mini in ${minimum[@]}; do
    minimum_output+="$mini,"
done
minimum_output+="$awk_path,$awk_path_size"


for avg in ${average[@]}; do
    avg=$(echo $(($avg / $count))) 
    average_output+="$avg,"
done
average_output+="$awk_path,$awk_path_size"

echo $minimum_output >> $LOG_DIR/$filename
echo $maximum_output >> $LOG_DIR/$filename
echo $average_output >> $LOG_DIR/$filename

chmod 400 $LOG_DIR/$filename