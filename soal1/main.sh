#!/bin/bash

readonly FILE="./users/user.txt"
readonly LOG="./log.txt"
readonly KEY=sisopmodul1
readonly TRUE=0
readonly FALSE=1


#mencari tahu user di user.txt ada di baris ke berapa
#return 0 jika tidak ditemukan
getUserRow(){
    if [ -f $FILE ]; then
        local val=$(awk -v str=$1 '
            BEGIN{
                i=0
            }

            {
                if(str == $1) {
                    i=NR
                }
            }

            END{print i}
        ' $FILE )

            return $val
    fi

    return 0
}

#decrypt password dan mengcompare password input dengan yang ada di user.txt
isMatchPasswd(){
    index=$1
    passwd=$2

    local hashPwd=$(awk -v i=$index 'FNR == i {print $NF}' $FILE)
    local realPwd=$(echo "$hashPwd" | openssl enc -d -des3 -base64 -pass pass:$KEY -pbkdf2)
    
    [[ $realPwd == $passwd ]] && return $TRUE || return $FALSE
}

#menulis log ke log.txt
writeLog(){
  message=$1
  user=$2

  # MM/DD/YY hh:mm:ss
  now=`date +%m/%d/%y\ %H:%M:%S`
  
  case "$message" in
    "ERROR") echo "$now LOGIN:$message Failed login attempt on user $user" >> ./log.txt || echo $user > ./log.txt ;;
    "INFO") echo "$now LOGIN:$message User $user logged in" >> ./log.txt || echo $user > ./log.txt ;;
  esac
}

#main code
echo "-LOGIN-"
userNow=""
passwdNow=""

while :
do
    printf "Username: "
    read user
    printf "Password: "
    read -s passwd
    printf "\n"

    getUserRow $user
    indexUser=$?

    if [[ $indexUser -eq 0 ]]; then
        printf "\nLogin Failed. Try Again.\n\n"
        continue
    fi


    isMatchPasswd $indexUser $passwd
    matchPasswd=$?

    case $matchPasswd in
    $TRUE) 
        writeLog "INFO" $user;
        printf "Login Success.\n\n"
        #userNow dipakai untuk command att/dl 
        userNow=$user
        passwdNow=$passwd
        break ;; 
    $FALSE)
        writeLog "ERROR" $user;
        printf "\nLogin Failed. Try Again.\n\n" ;;
    esac
done


checkAttempt(){
    local u=$1

    local success=$(grep -c "LOGIN:INFO User $u logged in" $LOG)
    local failed=$(grep -c "LOGIN:ERROR Failed login attempt on user $u" $LOG)

    # printf "user:$user\nsuccess: $success\nfailed: $failed\n\n"
    printf "user saat ini:$user\n"
    printf "total attempt: %d\n\n" $(($success + $failed))

    # let totalAttempt=$success+$failed
    
    # return totalAttempt
}

downloadImg(){
    local sum=$2
    local filename=$(printf "%s_%s" `date +%Y-%m-%d` $3)
    local passwd=$4

    if ! [[ "$sum" =~ ^[0-9]+$ ]]; then 
        echo "Not a number"
        return
    fi

    sudo apt-get wget

    # jika ternyata file nya sudah ada, unzip, tambahkan pic, zip lagi
    if [ -e "$filename" ]; then
        lastFileOrder=$(ls ./$filename | tail -1 | awk -F'_' '{print $2}' || echo 0)
        unzip -o $filename.zip -P $passwd
        
        for(( i=1; i<=$sum; i++ ))
            do
                order=$(printf "%02d" $(($lastFileOrder+$i)))
                wget -O ./$filename/PIC_$order https://loremflickr.com/320/240
            done
        
        zip $filename.zip -r $filename -P $passwd
        printf "\nTasks Done Successfully.\n\n"
    
    # jika file nya belum ada, create dir, tambahkan pic, zip
    else
        mkdir -p ./$filename

        for(( i=1; i <= $sum; i++ ))
            do
                order=$(printf "%02d" $i)
                wget -O ./$filename/PIC_$order https://loremflickr.com/320/240
            done
        
        zip $filename.zip -r $filename -P $passwd
        printf "\nTasks Done Successfully.\n\n"
    fi

}

#command = att / dl N, N = number
while :
do
    read cmd cmd2

    if [ $cmd == "att" ]; then
        checkAttempt $userNow
    elif [ $cmd == "dl" ]; then
        downloadImg $cmd $cmd2 $userNow $passwdNow
    else
        echo "command not found"
    fi
done




