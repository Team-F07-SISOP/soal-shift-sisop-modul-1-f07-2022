#!/bin/bash

# 2a
mkdir -p forensic_log_website_daffainfo_log

# 2b
average=$(awk -F ":" '
BEGIN{
    total=0; divider=0
} 
FNR == 1 {next} 
{ count[$3]++ }; 
{ hour[$3]++ } 
END { for (i in hour) total+=hour[i] } 
END{ for (i in hour) divider++ } 
END{ print "Rata-rata serangan adalah sebanyak", total/--divider, "requests per jam" }' log_website_daffainfo.log )

echo $average > ./forensic_log_website_daffainfo_log/ratarata.txt

# 2c
result=$(awk -F ':' '
BEGIN{
    count=0
    attackerIP=""
}
{ data[$1]++ } 
END{ 
    for (ip in data) 
        if(data[ip] > count) {
            count=data[ip];
            attackerIP=ip;
        } 
} 
END {
    print "IP yang paling banyak mengakses server adalah:", attackerIP, "sebanyak", count, "requests"
}' log_website_daffainfo.log )

echo $result > ./forensic_log_website_daffainfo_log/result.txt

# 2d
curl_req=$(awk -F ':' '
BEGIN{
    total=0
}
{ 
    if(index($NF, "curl") != 0){
        total++;
    }
}
END{ print "Ada", total, "requests yang menggunakan curl sebagai user-agent" }' log_website_daffainfo.log)

echo $curl_req >> ./forensic_log_website_daffainfo_log/result.txt


# 2e
ip_addr=$(awk -F [:/] '
{ 
    if($2 == "\"22" && $5 == 02)
        all_ip[$1]=$1
}
END{ 
    for(i in all_ip) printf "%s ", all_ip[i]
}
' log_website_daffainfo.log)

# IFS=split string ' '
IFS=' '

# all_ip akan membaca $ip_addr sebagai array
# dan setiap indeks ditentukan dari separator IFS
read -ra all_ip <<< "$ip_addr"

for i in "${all_ip[@]}"; do
    echo "$i Jam 2 pagi" >> ./forensic_log_website_daffainfo_log/result.txt
done
