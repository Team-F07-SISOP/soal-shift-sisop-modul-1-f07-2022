#!/bin/bash

readonly DATE_NOW=$(date "+%F%T" | tr -d "-" | tr -d ":")
readonly LOG_DIR=$HOME/log
readonly LOG_STARTER_STR="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"

is_cron_created=$(crontab -l | grep minute_log.sh)


# create crontab 
if [ -z "$is_cron_created" ]; then
    crontab -l > crontemp
    echo "* * * * * /bin/bash $PWD/minute_log.sh >/dev/null 2>&1" >> crontemp
    crontab crontemp
    rm crontemp

    echo "cron created"
fi

# 3a & 3b
mkdir -p $LOG_DIR

datenow=$(date "+%F%T" | tr -d "-" | tr -d ":")
filename="metrics_"$datenow".log"

touch $LOG_DIR/$filename

ram=$(
    free -m | 
    awk 'NR!=1 {str=$2; for(i=3;i<=NF;i++){str=str" "$i}; print str}' |
    tr " " , | tr "\n" ,
)

path_size=$(du -sh $HOME | awk '{print $2","$1}')

echo $LOG_STARTER_STR >> $LOG_DIR/$filename 
echo "$ram$path_size" >> $LOG_DIR/$filename

chmod 400 $LOG_DIR/$filename